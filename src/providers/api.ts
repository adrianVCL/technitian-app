import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable, ReplaySubject, throwError } from 'rxjs';
import { catchError, map, timeout } from 'rxjs/operators';
import {environment} from "../environments/environment";

@Injectable()

export class Api {

    private headers = [];
    private apiUrl;

    constructor(private http: HttpClient, private storage: Storage) {
        this.setApiUrl();
    }

    async setApiUrl() {
        const d = new Date();
        await this.http.get(environment.configUrl + '?d=' +  d).subscribe((data: any) => {
            this.apiUrl = data.demo.technitian.url;
            if (environment.production) {
                this.apiUrl = data.prod.technitian.url;
            }

            this.storage.set('api-url', this.apiUrl);
        });
    }

    getHeaders(): ReplaySubject<any> {
        const res = new ReplaySubject(1);

        this.storage.get('authorization').then((data) => {
            const nHeaders = {
                Authorization: ''
            };

            if (data) {
                nHeaders.Authorization = `Bearer ${data.access_token}`;
            }
            Object.assign(this.headers, nHeaders);
            res.next(this.headers);
        });
        return res;
    }

    post(url: string, body: object): ReplaySubject<any> {
        const response = new ReplaySubject<any>(1);

        this.getHeaders().subscribe((h: any) => {
            const headers = {
                Authorization: h.Authorization
            };

            this.http.post(
                `${this.apiUrl}${url}`,
                body,
                {
                    headers,
                    withCredentials: true
                }
            ).pipe(timeout(10000)).pipe(
                map((resp: any) => {
                    this.setLog(`${this.apiUrl}${url}`, body, 200, 1);
                    return {
                        code: 200,
                        response: resp
                    };
                })
            ).pipe(
                catchError((error: any) => {
                    this.setLog(`${this.apiUrl}${url}`, body, error.status, 2, error.error, error.detail);
                    return throwError({
                        code: error.status,
                        errorMessage: error.error,
                        detail: error.detail
                    });
                })
            ).subscribe(
                (res) => {
                    response.next(res);
                },
                (err) => {
                    response.next(err);
                }
            );
        });

        return response;
    }

    get(url: string, paramsData: any = null): ReplaySubject<any> {
        const response = new ReplaySubject<any>(1);
        this.getHeaders().subscribe((h: any) => {
            const headersData = new HttpHeaders({
                Authorization: h.Authorization
            });

            this.http.get(
                `${this.apiUrl}${url}`,
                {
                    headers: headersData,
                    withCredentials: true,
                    params: paramsData
                }
            ).pipe(timeout(30000))
                .pipe(map(res => res))
                .pipe(
                    catchError((error: any) => {
                        console.log('aqui estoy');
                        this.setLog(`${this.apiUrl}${url}`, paramsData, error.status, 2, error.error, error.detail);
                        return throwError({
                            elementDeleted: false,
                            errorMessage: error.error,
                            detail: error.detail,
                            code: error.status
                        });
                    })
                ).subscribe(
                    (res) => {
                        response.next(res);
                    },
                    (err) => {
                        response.next(err);
                    }
                );
        });

        return response;
    }

    put(url: string, body: object, isMultipart?): ReplaySubject<object> {
        const response = new ReplaySubject<any>(1);
        this.getHeaders().subscribe((h: any) => {
            const headers = new HttpHeaders({
                Authorization: h.Authorization
            });

            this.http.put(
                `${this.apiUrl}${url}`,
                body,
                {
                    headers,
                    withCredentials: true
                }
            ).pipe(
                map((resp: any) => {
                    this.setLog(`${this.apiUrl}${url}`, body, 200, 1);
                    return {
                        code: 200,
                        response: resp
                    };
                })
            ).pipe(
                catchError((error: any) => {
                    this.setLog(`${this.apiUrl}${url}`, body, error.status, 2, error.error, error.detail);
                    return throwError({
                        code: error.status,
                        errorMessage: error.error,
                        detail: error.detail
                    });
                })
            ).subscribe(
                (res) => {
                    response.next(res);
                },
                (err) => {
                    response.next(err);
                }
            );
        });

        return response;
    }

    destroy(url: string): ReplaySubject<any> {
        const response = new ReplaySubject<any>(1);
        this.getHeaders().subscribe((h: any) => {
            const headers = new HttpHeaders({
                Authorization: h.Authorization
            });

            this.http.delete(
                `${this.apiUrl}${url}`,
                {
                    headers
                }
            ).pipe(map(res => res))
                .pipe(
                    catchError((error: any) => {
                        return throwError({
                            elementDeleted: false,
                            errorMessage: error.error,
                            detail: error.detail
                        });
                    })
                ).subscribe(
                    (res) => {
                        response.next(res);
                    },
                    (err) => {
                        response.next(err);
                    }
                );
        });

        return response;
    }


    setLog(urlData: string, contentData: any, codeData: number, type: number, errorMessageData: any = null, detailData: any = null) {
        let typeLog = 'logs-error';
        if (type === 1) {
            typeLog = 'logs-info';
        }

        this.storage.get(typeLog).then((data) => {
            let logs = [];
            if (data) {
                logs = data;
            }

            logs.push({
                url: urlData,
                content: JSON.stringify(contentData),
                code: codeData,
                errorMessage: errorMessageData,
                detail: detailData
            });
            this.storage.set(typeLog, logs);
        });

    }
}
