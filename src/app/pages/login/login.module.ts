import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {LoginPage} from './login.page';
import {Api, Authorization} from '../../../providers';
import {HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/storage';
import {Deploy} from 'cordova-plugin-ionic/dist/ngx';

const routes: Routes = [
    {
        path: '',
        component: LoginPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ReactiveFormsModule,
        IonicStorageModule.forRoot(),
        RouterModule.forChild(routes),
        HttpClientModule
    ],
    declarations: [LoginPage],
    providers: [
        Api,
        Authorization,
        Deploy
    ]
})
export class LoginPageModule {
}
