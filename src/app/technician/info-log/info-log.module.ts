import { ShowErrorLogPage } from './../show-error-log/show-error-log.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { InfoLogPage } from './info-log.page';

const routes: Routes = [
  {
    path: '',
    component: InfoLogPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InfoLogPage, ShowErrorLogPage],
  entryComponents: [
    ShowErrorLogPage
  ]
})
export class InfoLogPageModule { }
