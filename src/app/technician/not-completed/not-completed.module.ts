import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NotCompletedPage } from './not-completed.page';
import {Api, Base64ToBlob, ErrorCodes, Loading, Tickets} from '../../../providers';
import {EditTicketPage} from '../edit-ticket/edit-ticket.page';
import {Camera} from '@ionic-native/camera/ngx';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {Diagnostic} from '@ionic-native/diagnostic/ngx';

const routes: Routes = [
  {
    path: '',
    component: NotCompletedPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule
  ],
  declarations: [NotCompletedPage, EditTicketPage],
  providers: [
    Loading,
    Api,
    ErrorCodes,
    Camera,
    Tickets,
    Geolocation,
    Base64ToBlob,
    Diagnostic
  ],
  entryComponents:[
      EditTicketPage
  ]
})
export class NotCompletedPageModule {}
