import {Component, OnInit} from '@angular/core';
import {Dialogs} from '@ionic-native/dialogs/ngx';
import {Authorization, ErrorCodes} from '../../providers';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-technician',
    templateUrl: './technician.page.html',
    styleUrls: ['./technician.page.scss'],
})
export class TechnicianPage implements OnInit {
    public appPages = [
        {
            title: 'Inicio',
            url: 'home',
            icon: 'home'
        },
        {
            title: 'Tickets Asignados',
            url: 'tickets',
            icon: 'list'
        },
        /*{
            title: 'Tickets Abiertos',
            url: 'opened',
            icon: 'list-box'
        },*/
        {
            title: 'Tickets Completados',
            url: 'completed',
            icon: 'hammer'
        },
        {
            title: 'Ticket Actual',
            url: 'current-job',
            icon: 'list'
        },
        {
            title: 'Tickets no enviados',
            url: 'not-completed',
            icon: 'bug'
        },
        /*{
            title: 'Manual Tecnico',
            url: 'manual',
            icon: 'list'
        },
        {
            title: 'Lista de partes',
            url: 'parts',
            icon: 'settings'
        },
        {
            title: 'Lista de Herramientas',
            url: 'tools',
            icon: 'build'
        },*/
        {
            title: 'Acerca de',
            url: 'about',
            icon: 'information-circle-outline'
        },
        {
            title: 'Logs',
            url: 'info-log',
            icon: 'warning'
        }
    ];

    constructor(private dialogs: Dialogs, private authorizationService: Authorization,
                private router: Router, private errorCodes: ErrorCodes, private storage: Storage) {
    }

    ngOnInit() {
        this.errorCodes.index().subscribe((data) => {
            if (!data.errorMessage && data.success) {
                this.storage.set('error_codes', data.response);
            }
        });
    }

    goState(state) {
        this.router.navigate(['/technician/' + state]);
    }

    closeSession() {
        // this.authorizationService.logout();
        // this.router.navigate(['/login']);
        this.dialogs.confirm('Esta seguro que desea cerrar su sesión', 'Cerrar Sesión', ['Si', 'Cancelar'])
            .then((confirm) => {
                if (confirm === 1) {
                    // this.storage.clear();
                    this.authorizationService.logout();
                    this.router.navigate(['/login']);
                }
            })
            .catch(e => console.log('Error displaying dialog', e));
    }

}
