import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Authorization} from '../../../providers/authorization';
import {SessionProvider} from '../../../providers';
import {Storage} from '@ionic/storage';
import {HttpClient} from '@angular/common/http';
import {Dialogs} from '@ionic-native/dialogs/ngx';
import {Deploy} from 'cordova-plugin-ionic/dist/ngx';
import {LoadingController} from "@ionic/angular";
import {load} from "@angular/core/src/render3";

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    user = {};
    loginForm: FormGroup;
    TECHNICIAN_ROL = 3;

    constructor(public router: Router, public formBuilder: FormBuilder, public authorizationProvider: Authorization,
                private sessionProvider: SessionProvider, private storage: Storage, private httpClient: HttpClient,
                private dialogs: Dialogs, private deploy: Deploy, private loadingController: LoadingController) {
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: [null, Validators.required],
            password: [null, Validators.required],
        });

        /*this.loginForm.get('email').setValue('gus.knul@hotmail.com');
        this.loginForm.get('password').setValue('123456');*/
    }

    async login() {
        const loading = await this.loadingController.create({
            message: 'Por favor espere un momento...',
        })
        await loading.present();
        if (this.loginForm.valid) {
            const params = this.loginForm.value;
            this.authorizationProvider.login(params).subscribe((data: any) => {
                loading.dismiss();
                if ('user' in data) {
                    if (this.isTechnician(data.user.roles)) {
                        this.storage.set('authorization', data);
                        this.router.navigate(['technician']);
                    } else {
                        this.showAlert('No cuenta con permisos para acceder a la aplicación');
                    }
                } else {
                    this.showAlert('Usuario o Contraseña Incorrectos');
                }
            }, error => {
                loading.dismiss();
                this.showAlert('Usuario o Contraseña Incorrectos');
            });
        } else {
            loading.dismiss();
            this.showAlert('Usuario y Contraseña Obligatorios');

        }
    }

    async downloadUpdate() {
        const loading = await this.loadingController.create({
            message: 'Actualizando aplicación...'
        });
        try {
            const update = await this.deploy.checkForUpdate();

            if (update.available) {
                // this.downloadProgress = 0;
                await loading.present();
                await this.deploy.downloadUpdate((progress) => {
                    console.log(progress);
                });
                await this.deploy.extractUpdate();
                await this.deploy.reloadApp();
                await loading.dismiss();
            }
        } catch (err) {
            await loading.dismiss();
            console.log(err);
            // We encountered an error.
            // Here's how we would log it to Ionic Pro Monitoring while also catching:
            // Pro.monitoring.exception(err);
        }
    }

    showAlert(message) {
        this.dialogs.alert(message, 'Alerta', 'Aceptar').then(() => {

        }).catch(e => {

        });
    }

    isTechnician(roles) {
        let hasRolTechnician = false;
        for (const rol of roles) {
            if (rol.id === this.TECHNICIAN_ROL) {
                hasRolTechnician = true;
            }
        }
        return hasRolTechnician;
    }
}
