import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IonContent, ModalController, NavParams} from '@ionic/angular';
import {Dialogs} from '@ionic-native/dialogs/ngx';
import {Base64ToBlob, ErrorCodes, Loading, Tickets} from '../../../providers';
import {Storage} from '@ionic/storage';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {Diagnostic} from '@ionic-native/diagnostic/ngx';
import {Router} from '@angular/router';
import {forEach} from "@angular-devkit/schematics";

@Component({
    selector: 'app-edit-ticket',
    templateUrl: './edit-ticket.page.html',
    styleUrls: ['./edit-ticket.page.scss'],
})
export class EditTicketPage implements OnInit {
    @ViewChild('pageTop') pageTop: IonContent;
    closeTicketForm: FormGroup;
    errorCodes = [];
    assignation;
    proofAddress;
    imagesServices = [];
    errors = [];
    completion;
    extraCharges;
    options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
    };


    constructor(private formBuilder: FormBuilder, private dialogs: Dialogs, private errorCodesService: ErrorCodes,
                private storage: Storage, private camera: Camera, private ticketService: Tickets,
                private geolocation: Geolocation, private base64ToBlob: Base64ToBlob, private diagnostic: Diagnostic,
                private modalController: ModalController, private router: Router, private loading: Loading, private navParams: NavParams) {
        this.closeTicketForm = this.formBuilder.group({
            serial_number: [''],
            recipient: ['', Validators.required],
            work_description: ['', Validators.required],
            latitude: [null, Validators.required],
            longitude: [null, Validators.required],
            id_error_codes: [null, Validators.required],
            tds_in: ['', Validators.required],
            tds_out: ['', Validators.required],
            extra_charges: [''],
            extra_charges_reasons: [''],
            used_parts: [],
            photos: [],
            proof_address: [],
            status: ['completed'],
            id_tickets: [],
            id_assignations: []
        });

        this.completion = this.navParams.get('completion');
        this.closeTicketForm.get('serial_number').setValue(this.completion.serial_number);
        this.closeTicketForm.get('recipient').setValue(this.completion.recipient);
        this.closeTicketForm.get('work_description').setValue(this.completion.work_description);
        this.closeTicketForm.get('latitude').setValue(this.completion.latitude);
        this.closeTicketForm.get('longitude').setValue(this.completion.longitude);
        this.closeTicketForm.get('id_error_codes').setValue(this.completion.id_error_codes);
        this.closeTicketForm.get('tds_in').setValue(this.completion.tds_in);
        this.closeTicketForm.get('tds_out').setValue(this.completion.tds_out);
        this.closeTicketForm.get('extra_charges').setValue(this.completion.extra_charges);
        this.closeTicketForm.get('extra_charges_reasons').setValue(this.completion.extra_charges_reasons);
        this.closeTicketForm.get('id_tickets').setValue(this.completion.id_tickets);
        this.closeTicketForm.get('id_assignations').setValue(this.completion.id_assignations);
        this.imagesServices = this.completion.photos;
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.loading.present();

        this.diagnostic.isLocationEnabled().then(state => {
            if (!state) {
                this.dialogs.alert('Debe habilitar su GPS para poder continuar', 'GPS APAGADO', 'ok').then(() => {
                    this.diagnostic.switchToLocationSettings();
                });
            }
        });

        this.storage.get('assignation').then((data) => {
            if (data !== null && data !== undefined) {
                this.assignation = data;
            }
        });


        this.errorCodesService.index().subscribe((data) => {
            if (!data.errorMessage && data.success) {
                this.errorCodes = data.response;
                this.storage.set('assignations', data.response);
            } else {
                this.storage.get('error_codes').then((errorCodes) => {
                    if (errorCodes) {
                        this.errorCodes = errorCodes;
                    }
                });
            }
            this.loading.dismiss();
        });

    }

    sentTicketClose() {
        this.errors = [];
        if (this.closeTicketForm.valid) {
            this.dialogs.confirm('¿Esta seguro que desea cerrar el Ticket?', 'Cerrar Ticket', ['Si', 'Cancelar'])
                .then((confirm) => {
                    if (confirm === 1) {
                        if (this.imagesServices.length > 0) {
                            const photos = [];
                            for (const imageService in this.imagesServices) {
                                if (imageService) {
                                    const t = this.base64ToBlob.base64ToBlob(this.imagesServices[imageService]);
                                    photos.push(t);
                                }
                            }
                            this.closeTicketForm.get('photos').setValue(photos);
                        }
                        const formData = new FormData();
                        const params = this.closeTicketForm.value;

                        for (const key in params) {
                            if (key !== 'photos') {
                                formData.append(key, params[key]);
                            } else {
                                for (const index in params[key]) {
                                    if (index) {
                                        formData.append('photos[]', params[key][index]);
                                    }
                                }
                            }
                        }
                        this.loading.present();
                        this.ticketService.setStatus(formData, this.completion.id_tickets).subscribe(data => {
                            this.loading.dismiss();
                            if (data.code === 200) {
                                this.dialogs.alert('Se completo el ticket', 'Exito', 'ok').then(() => {
                                    this.removeFailureJob();
                                    this.closeModal();
                                });
                            } else {
                                if (data.code === 400) {
                                    const errors = data.errorMessage.response.errors;
                                    for (const error in errors) {
                                        if (error) {
                                            for (const errorData in errors[error]) {
                                                if (errorData) {
                                                    this.errors.push({
                                                        name: error,
                                                        error: errors[error][errorData]
                                                    });
                                                }
                                            }
                                        }
                                    }
                                    this.pageTop.scrollToTop();
                                } else {
                                    this.storage.get('completed-failures').then((job) => {
                                        let jobs = [];
                                        if (job) {
                                            jobs = job;
                                        }
                                        jobs.push(params);
                                        this.storage.set('completed-failures', jobs);
                                    });

                                    this.dialogs.alert('El ticket se actualizara automaticamente' +
                                        ' cuando cuente con conexión a internet', 'Exito', 'ok').then(() => {
                                        this.removeFailureJob();
                                    });
                                }
                            }
                        });
                    }
                })
                .catch(e => console.log('Error displaying dialog', e));
        } else {
            this.dialogs.alert('Debe llenar los campos obligatorios', 'Invalido', 'Ok');
        }

    }

    getImageService() {
        this.selectFile(true);
    }

    getProofAddress() {
        this.selectFile(false);
    }

    selectFile(isMultiple) {
        const that = this;
        this.camera.getPicture(this.options).then((imageData) => {
            if (isMultiple) {
                this.imagesServices.push(imageData);
            } else {
                this.proofAddress = imageData;
            }
        }, (err) => {

        });
    }

    dataURItoBlob(dataURI) {
        let byteString;
        byteString = atob(dataURI);

        const ia = new Uint8Array(byteString.length);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type: 'image/png'});
    }

    removeProofAddress() {
        this.proofAddress = null;
    }

    removeImageServices(index) {
        this.imagesServices.splice(index, 1);
    }

    getLocation() {
        this.geolocation.getCurrentPosition().then((resp) => {
            this.closeTicketForm.get('latitude').setValue(resp.coords.latitude);
            this.closeTicketForm.get('longitude').setValue(resp.coords.longitude);
        }).catch((error) => {
        });
    }

    closeModal() {
        this.modalController.dismiss({
            reload: true
        });
    }

    removeFailureJob() {
        this.storage.get('completed-failures').then((completedFailures) => {
            if (completedFailures) {
                let i = 0;
                let indexElement = -1;
                for (const data of completedFailures) {
                    console.log()
                    if (data.assignation.id_assignations === this.completion.id_assignations) {
                        indexElement = i;
                    }
                    i++;
                }
                console.log(i);
                if (indexElement >= 0) {
                    completedFailures.splice(indexElement, 1);
                    this.storage.set('completed-failures', completedFailures);
                }
            }
        });
    }

    setExtraCharges(event) {
        this.closeTicketForm.get('extra_charges').setValue('');
        this.closeTicketForm.get('extra_charges_reasons').setValue('');
        if (event.detail.checked) {
            this.closeTicketForm.get('extra_charges').setValidators([Validators.required]);
            this.closeTicketForm.get('extra_charges_reasons').setValidators([Validators.required]);
            this.closeTicketForm.get('extra_charges').updateValueAndValidity();
            this.closeTicketForm.get('extra_charges_reasons').updateValueAndValidity();
        } else {
            this.closeTicketForm.get('extra_charges').setValidators([]);
            this.closeTicketForm.get('extra_charges_reasons').setValidators([]);
            this.closeTicketForm.get('extra_charges').updateValueAndValidity();
            this.closeTicketForm.get('extra_charges_reasons').updateValueAndValidity();
        }
    }

}
