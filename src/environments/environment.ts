// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: true,
    //apiUrl: 'http://192.168.1.235/aguagente/api/public/', //home
    //apiUrl: 'http://192.168.100.29/aguagente/api/public/',//office
    configUrl: 'https://www.aguagente.com/config/config.json',
    //apiUrl: 'http://panelaguagente.xyz/demo/api52/public/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
