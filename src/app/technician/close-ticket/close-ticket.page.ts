import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {Dialogs} from '@ionic-native/dialogs/ngx';
import {Base64ToBlob, ErrorCodes, Loading, Tickets} from '../../../providers';
import {Storage} from '@ionic/storage';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {Diagnostic} from '@ionic-native/diagnostic/ngx';
import {IonContent, ModalController} from '@ionic/angular';
import {Router} from '@angular/router';
import {getTranslationConstPrefix} from '@angular/compiler/src/render3/view/i18n/util';

@Component({
    selector: 'app-close-ticket',
    templateUrl: './close-ticket.page.html',
    styleUrls: ['./close-ticket.page.scss']
})
export class CloseTicketPage implements OnInit {
    @ViewChild('pageTop') pageTop: IonContent;
    closeTicketForm: FormGroup;
    errorCodes = [];
    assignation;
    proofAddress;
    imagesServices = [];
    errors = [];
    extraCharges = false;

    options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
    };

    constructor(private formBuilder: FormBuilder, private dialogs: Dialogs, private errorCodesService: ErrorCodes,
                private storage: Storage, private camera: Camera, private ticketService: Tickets,
                private geolocation: Geolocation, private base64ToBlob: Base64ToBlob, private diagnostic: Diagnostic,
                private modalController: ModalController, private router: Router, private loading: Loading) {

    }

    ngOnInit() {
        this.closeTicketForm = this.formBuilder.group({
            serial_number: [''],
            recipient: ['', Validators.required],
            work_description: ['', Validators.required],
            latitude: [null, Validators.required],
            longitude: [null, Validators.required],
            id_error_codes: [null, Validators.required],
            tds_in: ['', Validators.required],
            tds_out: ['', Validators.required],
            extra_charges: [''],
            extra_charges_reasons: [''],
            used_parts: [],
            photos: [],
            proof_address: [],
            status: ['completed'],
            id_tickets: [],
            id_assignations: []
        });
        this.getLocation();
    }

    ionViewWillEnter() {
        this.loading.present();

        this.diagnostic.isLocationEnabled().then(state => {
            if (!state) {
                this.dialogs.alert('Debe habilitar su GPS para poder continuar', 'GPS APAGADO', 'ok').then(() => {
                    this.diagnostic.switchToLocationSettings();
                });
            }
        });

        this.storage.get('assignation').then((data) => {
            if (data !== null && data !== undefined) {
                this.assignation = data;
            }
        });


        this.errorCodesService.index().subscribe((data) => {
            if (!data.errorMessage && data.success) {
                this.errorCodes = data.response;
                this.storage.set('error_codes', data.response);
            } else {
                this.storage.get('error_codes').then((errorCodes) => {
                    if (errorCodes) {
                        this.errorCodes = errorCodes;
                    }
                });
            }
            this.loading.dismiss();
        });

    }

    sentTicketClose() {
        this.closeTicketForm.get('id_assignations').setValue(this.assignation.id_assignations);
        this.closeTicketForm.get('id_tickets').setValue(this.assignation.ticket.id_tickets);
        this.errors = [];
        if (this.closeTicketForm.valid) {
            this.dialogs.confirm('¿Esta seguro que desea cerrar el Ticket?', 'Cerrar Ticket', ['Si', 'Cancelar'])
                .then((confirm) => {
                    if (confirm === 1) {
                        if (this.imagesServices.length > 0) {
                            const photos = [];
                            for (const imageService in this.imagesServices) {
                                if (imageService) {
                                    const t = this.base64ToBlob.base64ToBlob(this.imagesServices[imageService]);
                                    photos.push(t);
                                }
                            }
                            this.closeTicketForm.get('photos').setValue(photos);
                        }
                        const formData = new FormData();
                        const params = this.closeTicketForm.value;

                        for (const key in params) {
                            if (key !== 'photos') {
                                formData.append(key, params[key]);
                            } else {
                                for (const index in params[key]) {
                                    if (index) {
                                        formData.append('photos[]', params[key][index]);
                                    }
                                }
                            }
                        }
                        this.loading.present();
                        this.ticketService.setStatus(formData, this.assignation.ticket.id_tickets).subscribe(data => {
                            this.loading.dismiss();
                            if (data.code === 200) {
                                this.dialogs.alert('Se completo el ticket', 'Exito', 'ok').then(() => {
                                    this.storage.remove('assignation');
                                    this.closeModal();
                                    this.router.navigate(['technician/tickets']);
                                });
                            } else {
                                if (data.code === 400) {
                                    const errors = data.errorMessage.response.errors;
                                    for (const error in errors) {
                                        if (error) {
                                            for (const errorData in errors[error]) {
                                                if (errorData) {
                                                    this.errors.push({
                                                        name: error,
                                                        error: errors[error][errorData]
                                                    });
                                                }
                                            }
                                        }
                                    }
                                    this.pageTop.scrollToTop();
                                } else {
                                    this.storage.get('completed-failures').then((job) => {
                                        let jobs = [];
                                        if (job) {
                                            jobs = job;
                                        }
                                        params.photos = this.imagesServices;
                                        jobs.push({
                                            completion: params,
                                            assignation: this.assignation
                                        });
                                        console.log(params);
                                        this.storage.set('completed-failures', jobs);
                                    });

                                    this.dialogs.alert('El ticket se actualizara automaticamente' +
                                        ' cuando cuente con conexión a internet', 'Exito', 'ok').then(() => {
                                        this.storage.get('assignations').then((assignations) => {
                                            if (assignations) {
                                                let i = 0;
                                                let indexElement = -1;
                                                for (const assignation of assignations) {
                                                    if (assignation.id_assignations === this.assignation.id_assignations) {
                                                        indexElement = i;
                                                    }
                                                    i++;
                                                }

                                                if (indexElement >= 0) {
                                                    assignations.splice(indexElement, 1);
                                                    this.storage.set('assignations', assignations);
                                                }
                                            }
                                            this.storage.remove('assignation');
                                            this.closeModal();
                                            this.router.navigate(['technician/tickets']);
                                        });
                                    });
                                }
                            }
                        });
                    }
                })
                .catch(e => console.log('Error displaying dialog', e));
        } else {
            this.errors = this.getErrors();
            this.pageTop.scrollToTop();
            this.dialogs.alert('Debe llenar los campos obligatorios', 'Invalido', 'Ok');
        }

    }

    getErrors() {
        const result = [];
        Object.keys(this.closeTicketForm.controls).forEach(key => {

            const controlErrors: ValidationErrors = this.closeTicketForm.get(key).errors;
            if (controlErrors) {
                Object.keys(controlErrors).forEach(keyError => {
                    result.push({
                        name: key,
                        error: keyError,
                        value: controlErrors[keyError]
                    });
                });
            }
        });

        return result;
    }

    getImageService() {
        this.selectFile(true);
    }

    getProofAddress() {
        this.selectFile(false);
    }

    selectFile(isMultiple) {
        const that = this;
        this.camera.getPicture(this.options).then((imageData) => {
            if (isMultiple) {
                this.imagesServices.push(imageData);
            } else {
                this.proofAddress = imageData;
            }
        }, (err) => {
            // this.getToast("No se pudo cargar la imagen");
        });
    }

    dataURItoBlob(dataURI) {
        let byteString;
        byteString = atob(dataURI);

        const ia = new Uint8Array(byteString.length);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type: 'image/png'});
    }

    removeProofAddress() {
        this.proofAddress = null;
    }

    removeImageServices(index) {
        this.imagesServices.splice(index, 1);
    }

    getLocation() {

        this.diagnostic.isLocationEnabled().then(state => {
            if (!state) {
                this.dialogs.alert('Debe habilitar su GPS para poder continuar', 'GPS APAGADO', 'ok').then(() => {
                    this.diagnostic.switchToLocationSettings();
                });
            } else {
                this.geolocation.getCurrentPosition().then((resp) => {
                    this.closeTicketForm.get('latitude').setValue(resp.coords.latitude);
                    this.closeTicketForm.get('longitude').setValue(resp.coords.longitude);
                }).catch((error) => {
                });
            }
        });
    }

    closeModal() {
        this.modalController.dismiss();
    }

    setExtraCharges(event) {
        this.closeTicketForm.get('extra_charges').setValue('');
        this.closeTicketForm.get('extra_charges_reasons').setValue('');
        if (event.detail.checked) {
            this.closeTicketForm.get('extra_charges').setValidators([Validators.required]);
            this.closeTicketForm.get('extra_charges_reasons').setValidators([Validators.required]);
            this.closeTicketForm.get('extra_charges').updateValueAndValidity();
            this.closeTicketForm.get('extra_charges_reasons').updateValueAndValidity();
        } else {
            this.closeTicketForm.get('extra_charges').setValidators([]);
            this.closeTicketForm.get('extra_charges_reasons').setValidators([]);
            this.closeTicketForm.get('extra_charges').updateValueAndValidity();
            this.closeTicketForm.get('extra_charges_reasons').updateValueAndValidity();
        }
    }
}
