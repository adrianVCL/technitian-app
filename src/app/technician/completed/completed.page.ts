import {Component, OnInit} from '@angular/core';
import {Tickets, Loading} from '../../../providers';
import {ModalController} from '@ionic/angular';
import {ShowTicketPage} from '../show-ticket/show-ticket.page';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-completed',
    templateUrl: './completed.page.html',
    styleUrls: ['./completed.page.scss'],
})
export class CompletedPage implements OnInit {

    completed = [];
    beginDateOptions: any;
    endDateOptions: any;
    today = new Date();
    beginDate;
    endDate;

    constructor(private ticketsService: Tickets, private loadingService: Loading, private modalController: ModalController,
                private storage: Storage) {
        const beginDateFormat = new Date();
        beginDateFormat.setMonth(this.today.getMonth() - 1);

        this.beginDate = beginDateFormat.toISOString();
        this.endDate = this.today.toISOString();
    }

    ngOnInit() {
    }

    setBeginDate($event) {
        this.beginDate  = $event.detail.value;
        this.refreshTickets(null);
    }

    setEndDate($event) {
        this.endDate = $event.detail.value;
        this.refreshTickets(null);
    }

    async showTicket(ticketData) {
        const modal = await this.modalController.create({
            component: ShowTicketPage,
            componentProps: {
                ticket: ticketData
            }
        });
        return await modal.present();
    }

    ionViewWillEnter() {
        this.refreshTickets(null);
    }

    refreshTickets($event) {
        this.loadingService.present();
        this.ticketsService.index({
            status: 'completed,closed,confirmed',
            begin_date: this.beginDate,
            end_date: this.endDate
        }).subscribe((data: any) => {
            if (!data.errorMessage && data.success) {
                this.completed = data.response.filter(items => (items.ticket.status === 'completed' || items.ticket.status === 'closed'));
                this.storage.set('completed', data.response.filter(items => (items.ticket.status === 'completed'
                    || items.ticket.status === 'closed')));
            } else {
                if (data.code !== 401) {
                    this.storage.get('completed').then((completed) => {
                        if (completed) {
                            this.completed = completed.filter(items => (items.ticket.status === 'completed'
                                || items.ticket.status === 'closed'));
                        }

                    });
                }
            }

            setTimeout(() => {
                if ($event !== null) {
                    $event.target.complete();
                }
                this.loadingService.dismiss();
            }, 2000);
        });
    }

}
