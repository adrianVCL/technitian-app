import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {TechnicianPage} from './technician.page';
import {Api, ErrorCodes} from '../../providers';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'home'
    },
    {
        path: '',
        component: TechnicianPage,
        children: [
            {
                path: 'home',
                loadChildren: './home/home.module#HomePageModule'
            },
            {
                path: 'tickets',
                loadChildren: './tickets/tickets.module#TicketsPageModule'
            },
            {
                path: 'about',
                loadChildren: './about/about.module#AboutPageModule'
            },
            {
                path: 'tools',
                loadChildren: './tools/tools.module#ToolsPageModule'
            },
            {
                path: 'parts',
                loadChildren: './parts/parts.module#PartsPageModule'
            },
            {
                path: 'manual',
                loadChildren: './manual/manual.module#ManualPageModule'
            },
            {
                path: 'current-job',
                loadChildren: './current-job/current-job.module#CurrentJobPageModule'
            },
            {
                path: 'close-ticket',
                loadChildren: './close-ticket/close-ticket.module#CloseTicketPageModule'
            },
            {
                path: 'completed',
                loadChildren: './completed/completed.module#CompletedPageModule'
            },
            {
                path: 'opened',
                loadChildren: './opened/opened.module#OpenedPageModule'
            },
            {
                path: 'not-completed',
                loadChildren: './not-completed/not-completed.module#NotCompletedPageModule'
            },
            {
                path: 'info-log',
                loadChildren: './info-log/info-log.module#InfoLogPageModule'
            },
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [TechnicianPage],
    providers: [
        Api,
        ErrorCodes
    ]
})
export class TechnicianPageModule {
}
