import { Injectable } from '@angular/core';
import { Api } from './api';
import { BehaviorSubject, ReplaySubject, Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class Authorization {
    authState = new BehaviorSubject(false);
    apiUrl;

    constructor(private apiService: Api, private storage: Storage, private readonly http: HttpClient) {
        this.setApiUrl();
        this.ifLoggedIn();
    }

    async setApiUrl() {
        await this.storage.get('api-url').then(data => {
            console.log(data);
            if (data) {
                this.apiUrl = data;
            }
        });
    }

    login(credentials): Observable<Object> {
        return this.http.post(`${this.apiUrl}auth/login`, credentials, { withCredentials: true });
    }

    // login(credentials) {
    //     const response = new ReplaySubject<any>(1);
    //     this.apiService.post('auth/login', credentials).subscribe((data: any) => {
    //         if (data.code === 200) {
    //             this.authState.next(true);
    //         }
    //         response.next(data);
    //     });
    //     return response;
    // }

    ifLoggedIn() {
        this.storage.get('authorization').then((response) => {
            if (response) {
                this.authState.next(true);
            }
        });
    }

    logout() {
        this.storage.remove('authorization');
    }

    isAuthenticated() {
        return this.authState.asObservable();
    }

    isTechnician(roles) {
        for (const rol in roles) {

        }
        return this.authState.value;
    }
}
