import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonCol, IonicModule} from '@ionic/angular';

import {CloseTicketPage} from './close-ticket.page';
import {Api, Base64ToBlob, ErrorCodes, Tickets} from '../../../providers';
import {Camera} from '@ionic-native/camera/ngx';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

const routes: Routes = [
    {
        path: '',
        component: CloseTicketPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [],
    providers: [

    ]
})
export class CloseTicketPageModule {
}
