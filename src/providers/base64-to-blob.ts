import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class Base64ToBlob {


    constructor() {
    }


    base64ToBlob(base64) {
        let byteString;
        byteString = atob(base64);

        const ia = new Uint8Array(byteString.length);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type: 'image/png'});
    }

}
