import {Component} from '@angular/core';

import {LoadingController, Platform, ToastController} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Authorization, Base64ToBlob, Loading, SessionProvider, Tickets} from '../providers';
import {Storage} from '@ionic/storage';
import {Router} from '@angular/router';
import {Network} from '@ionic-native/network/ngx';
import {Diagnostic} from '@ionic-native/diagnostic/ngx';
import {Dialogs} from '@ionic-native/dialogs/ngx';
import {Deploy} from 'cordova-plugin-ionic/dist/ngx';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import {BugsnagErrorHandler} from "@bugsnag/plugin-angular";

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {
    toast;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private sessionProvider: SessionProvider,
        private storage: Storage,
        private router: Router,
        private authorization: Authorization,
        private network: Network,
        private toastController: ToastController,
        private ticketsService: Tickets,
        private diagnostic: Diagnostic,
        private dialogs: Dialogs,
        private loading: Loading,
        private loadingController: LoadingController,
        private base64ToBlob: Base64ToBlob,
        private deploy: Deploy,
        private http: HttpClient
    ) {
        storage.get('authorization').then(data => {
           //console.log(data);
        });
        const d = new Date();
        this.http.get(environment.configUrl+ '?d=' +  d).subscribe((data: any) => {

            this.storage.set('api-url', data.demo.technitian.url);
            if (environment.production) {
                this.storage.set('api-url', data.prod.technitian.url);
            }
            this.initializeApp();
            this.showNetworkState();
        });
    }

    async showNetworkState() {
        this.toast = await this.toastController.create({
            message: 'No hay conexión a Internet',
            color: 'danger',
            position: 'top',
            cssClass: 'network-toast'
        });
    }

    /*async changeToBetaChannel() {
        await this.deploy.configure({channel: 'Master'});
    }*/

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();

            this.downloadUpdate();

            this.network.onDisconnect().subscribe(() => {
                console.log('desconectado');
                this.toast.present();
            });

            this.network.onConnect().subscribe(() => {
                this.toast.dismiss();
                this.showNetworkState();
                this.sentFailureJobs();
            });

            this.diagnostic.isLocationAvailable().then(state => {
                if (state === this.diagnostic.locationMode.LOCATION_OFF) {
                    this.dialogs.alert('Debe encender su GPS para poder continuar', 'GPS APAGADO', 'ok');
                }
            });

            this.authorization.authState.subscribe(state => {
                if (state) {
                    this.router.navigate(['technician']);
                } else {
                    this.router.navigate(['login']);
                }
            });

        });
    }

    async downloadUpdate() {
        const loading = await this.loadingController.create({
            message: 'Actualizando aplicación...'
        });
        try {
            const update = await this.deploy.checkForUpdate();

            if (update.available) {
                // this.downloadProgress = 0;
                await loading.present();
                await this.deploy.downloadUpdate((progress) => {
                    console.log(progress);
                });
                await this.deploy.extractUpdate();
                await this.deploy.reloadApp();
                await loading.dismiss();
            }
        } catch (err) {
            await loading.dismiss();
            console.log(err);
            // We encountered an error.
            // Here's how we would log it to Ionic Pro Monitoring while also catching:
            // Pro.monitoring.exception(err);
        }
    }

    sentFailureJobs() {
        this.storage.get('completed-failures').then(data => {
            if (data) {
                const jobs = [];
                this.dialogs.alert('Debe sincronizar la información antes de continuar', 'Sincronizar',
                    'Comenzar Sincronización').then(() => {
                    this.loading.present();
                    let successData = 0;
                    for (const job of data) {
                        const formData = new FormData();
                        const completion = job.completion;
                        for (const key in completion) {
                            if (key !== 'photos') {
                                formData.append(key, completion[key]);
                            } else {
                                for (const index in completion[key]) {
                                    if (index) {
                                        const t = this.base64ToBlob.base64ToBlob(completion[key][index]);
                                        formData.append('photos[]', t);
                                    }
                                }
                            }
                        }

                        this.ticketsService.setStatus(formData, completion.id_tickets).subscribe(dataResponse => {
                            console.log(dataResponse);
                            if (dataResponse.code === 200) {
                                successData++;
                            } else {
                                if (data.code === 400) {
                                    const errorDataArray = [];
                                    const errors = data.errorMessage.response.errors;
                                    for (const error in errors) {
                                        if (error) {
                                            for (const errorData in errors[error]) {
                                                if (errorData) {
                                                    errorDataArray.push({
                                                        name: error,
                                                        error: errors[error][errorData]
                                                    });
                                                }
                                            }
                                        }
                                    }
                                    job.errors = errorDataArray;
                                }
                                jobs.push(job);
                            }
                        });
                    }

                    setTimeout(() => {
                        this.loading.dismiss();

                        this.dialogs.alert('Se sincronizaron ' + successData + ' de ' + data.length +
                            ' para ver los trabajos no enviados verifique en el menu TICKETS NO COMPLETADOS', 'Sincronización terminada',
                            'Ok').then(() => {
                            this.storage.set('completed-failures', jobs);
                        });
                    }, 10000);

                });
            }
        });
    }
}
